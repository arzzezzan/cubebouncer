using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{
    public GameObject[] enemyPrefab;
    private float spawnRange = 9f;

    public int enemyCount;
    public int waveCount = 1;

    public GameObject[] powerupPrefabs;
    public GameObject bossPrefab;
    public GameObject[] miniBossFighters;
    private int round;
    private bool isGameOver;
    void Start()
    {
        isGameOver = false;
        round = 1;
        int randomPowerup = Random.Range(0, powerupPrefabs.Length); 
        
    }

    // Update is called once per frame
    void Update()
    {
        WaveControl();
    }
    void SpawnEnemyWave(int enemiesToSpawn)
    {
        if (round < 10)
        {
            for (int i = 0; i < enemiesToSpawn; i++)
            {
                int randomEnemy = Random.Range(0, enemyPrefab.Length);
                Instantiate(enemyPrefab[randomEnemy], RandomPosSpawn(), enemyPrefab[randomEnemy].transform.rotation);
            }
        }
            
        if(round == 10)
        {
            Instantiate(bossPrefab, RandomPosSpawn(), bossPrefab.transform.rotation);
            for (int i = 0; i < enemiesToSpawn / 2; i++)
            {

                int randomEnemy = Random.Range(0, miniBossFighters.Length);
                Instantiate(miniBossFighters[randomEnemy], RandomPosSpawn(), miniBossFighters[randomEnemy].transform.rotation);
            }
        }
        
    }
    void WaveControl()
    {
        if (!isGameOver)
        {
            if(round == 10)
            {
                GameOver();
            }
            enemyCount = FindObjectsOfType<Enemy>().Length;
            if (enemyCount == 0)
            {
                waveCount++;
                round++;
                SpawnEnemyWave(waveCount);
                int randomPowerup = Random.Range(0, powerupPrefabs.Length);
                Instantiate(powerupPrefabs[randomPowerup], RandomPosSpawn(), powerupPrefabs[randomPowerup].transform.rotation);

            }
        }
    }
    private Vector3 RandomPosSpawn()
    {
        enemyCount = FindObjectsOfType<Enemy>().Length;
        float spawnPosX = Random.Range(-spawnRange, spawnRange);
        float spawnPosZ = Random.Range(-spawnRange, spawnRange);
        Vector3 randomPos = new Vector3(spawnPosX, 0f, spawnPosZ);
        return randomPos;
    }
    private void GameOver()
    {
        isGameOver = true;
    }
 
}
