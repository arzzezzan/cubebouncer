using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed;
    [SerializeField]private float powerupStrenght = 17f;
    private Rigidbody rb;

    public bool isPowerup = false;

    private GameObject focalPoint;
    public GameObject powerupIndicator;

    public PowerUpType currentPowerUp = PowerUpType.None; 

    public GameObject rocketPrefab; 
    private GameObject tmpRocket; 
    private Coroutine powerupCountdown;

    public float smashSpeed; 
    public float explosionForce; 
    public float explosionRadius; 
    private bool smashing = false; 
    [SerializeField] private int countOfSmash = 1;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        focalPoint = GameObject.Find("FocalPoint");
    }

    // Update is called once per frame
    void Update()
    {
        PlayerMovement();
        PositionOfPowerup();
        RocketControll();
        SmashControll();
    }
    void PlayerMovement()
    {
        float verticalInput = Input.GetAxis("Vertical");
        rb.AddForce(focalPoint.transform.forward * verticalInput * speed);
    }
    void PositionOfPowerup()
    {
        powerupIndicator.transform.position = new Vector3(transform.position.x, transform.position.y + (-0.47f), transform.position.z);
    }
    void RocketControll()
    {
        if (currentPowerUp == PowerUpType.Rockets && Input.GetKeyDown(KeyCode.F)) 
        { 
            LaunchRockets(); 
        }
    }
    void SmashControll()
    {
        if (currentPowerUp == PowerUpType.Smash && Input.GetKeyDown(KeyCode.Space) && !smashing) 
        {
            smashing = true;
            Smashing();
            
           
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Powerup"))
        {
            isPowerup = true;
            currentPowerUp = other.gameObject.GetComponent<PowerUp>().powerUpType;
            powerupIndicator.SetActive(true);
            Destroy(other.gameObject);
            if (powerupCountdown != null) 
            { 
                StopCoroutine(powerupCountdown); 
            }
            powerupCountdown = StartCoroutine(PowerupDuration());
            
        }
    }
    IEnumerator PowerupDuration()
    {
        yield return new WaitForSeconds(7f);
        isPowerup = false;
        currentPowerUp = PowerUpType.None;
        powerupIndicator.SetActive(false);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy") && currentPowerUp == PowerUpType.Pushback)
        {
            Rigidbody enemyBody = collision.gameObject.GetComponent<Rigidbody>();
            Vector3 magnitude =(enemyBody.transform.position - transform.position);

            enemyBody.AddForce(magnitude * powerupStrenght, ForceMode.Impulse);
        }
        if(collision.gameObject.CompareTag("Island") && currentPowerUp == PowerUpType.Smash)
        {
            var enemies = FindObjectsOfType<Enemy>();


            for (int i = 0; i < enemies.Length; i++)
            {
                if (enemies[i] != null)
                    enemies[i].GetComponent<Rigidbody>().AddExplosionForce(explosionForce, transform.position,
                        explosionRadius, 0.0f, ForceMode.Impulse);
            }
            smashing = false;
            if (countOfSmash == 0)
            {
                currentPowerUp = PowerUpType.None;
            }

        }
    }
    void LaunchRockets() 
    { 
        foreach (var enemy in FindObjectsOfType<Enemy>()) 
        { 
            tmpRocket = Instantiate(rocketPrefab, transform.position + Vector3.up, Quaternion.identity); 
            tmpRocket.GetComponent<RocketBehaviour>().Fire(enemy.transform); 
        } 
    }
    void Smashing()
    {
        rb.AddForce(Vector3.up * smashSpeed, ForceMode.Impulse);
        countOfSmash -= 1;

    }
}
