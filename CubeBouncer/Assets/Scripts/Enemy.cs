using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    private Rigidbody rb;
    private GameObject player;
    public float speed = 4.0f;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        player = GameObject.Find("Player");
    }

    // Update is called once per frame
    void Update()
    {
        EnemyMove();
        OutOfBound();
    }
    void EnemyMove()
    {
        rb.AddForce((player.transform.position - transform.position).normalized * speed );
        
    }
    void OutOfBound()
    {
        if (transform.position.y < -5f)
        {
            Destroy(gameObject);
        }
    }
}
